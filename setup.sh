#!/bin/bash

# Small script for installing dot-files. Does **NOT** install Firefox UserChrome, as you can have multiple Firefox profiles.

current_path=$(realpath `dirname $BASH_SOURCE`)
echo "Path of the repository: $current_path"
files=`ls $current_path/config`

ask() {
    echo -n "$1 [Y/n] "
    read input
    if [[ $input =~ ^(y|Y)?$ ]]; then
        return 0
    fi
    return 1
}

ln_dir_content() {
    source=$2
    target=$3
    ask "Use $3 as $1 directory?" || eval 'echo "Exiting..." && exit 1'
    files=`ls $source`
    for file in $files; do
        ask "Install $file ?" || continue
        echo $source/$file $target
        ln -s $source/$file $target 2> /dev/null \
        && echo "Successfully created symbolic link for $file" \
        || echo "Failed to create symbolic link for $file"
    done


}

config=$XDG_CONFIG_HOME
if [ ! -d "$dir" ]; then    
    config=$HOME/.config
    echo "Environment variable \$XDG_CONFIG_HOME not found or invalid, falling back to $config"
fi

bin=$HOME/.local/bin

ln_dir_content "config" "$PWD/config" "$config"
ln_dir_content "bin" "$PWD/bin" "$bin" 
